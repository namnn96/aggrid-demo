// core
import { Injectable } from '@angular/core';

// models
import { IAuthenticationCredentials } from '@app/auth/models';

@Injectable()
export class AuthService {

  constructor() {}

  getCredentials(): IAuthenticationCredentials {
    return this.getMockCredentials();
  }

  logout(): void {

  }

  getLegacyTokens(): void {

  }

  getXHR(): any {

  }

  getToken(): string {
    return this.getMockToken();
  }

  private getMockToken(): string {
    return 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IjlkYTYwZmY0LTNjMzYtNDg5Zi05NGJlLWRjZjM2YzgwZDE2ZCJ9.eyJleHAiOjE1ODc4ODAxMzgsImZpcm1JZCI6IjUzYjYzZmRlLTJjZDQtNDgyOS1hOGIwLTg3NmVmNzQ5ZTY5MSIsInVzZXJJZCI6IjNiM2MwY2IwLWQzYjYtNDFkOS1hMmM3LTE4M2E4NzZlM2U3YSIsInN0YWZmSWQiOiI0ZWZhZjlhZC1iNzIwLTRiN2UtYWJiMy0yNTdmZjIwODQyMmUiLCJzZXNzaW9uSWQiOiI3RUpyZ2NfMDhlZTcwZmEtYTkzMi00MGExLTg5MTQtN2QwYWIwNGJhNGIyIiwic2NvcGUiOiIqIiwiYnJhbmQiOiJMRUFQIiwicmVnaW9uIjoiYXUiLCJlbnZpcm9ubWVudCI6InRlc3QiLCJ1c2VyVHlwZSI6IkxlYXAiLCJwcm92aWRlciI6ImxlYXAtdXNlciIsImFwaV9wZXJtcyI6W3siciI6IioiLCJtIjpbIioiXX1dLCJhdWQiOiJNQlVIUElGRUlWSjhDTkgwIiwiaXNzIjoiYXV0aC10ZXN0LmxlYXAuc2VydmljZXMiLCJzdWIiOiIzYjNjMGNiMC1kM2I2LTQxZDktYTJjNy0xODNhODc2ZTNlN2EiLCJqdGkiOiJSUnhZeVNzVU54aXZmNkhEIn0.N31NbLxOXMDvnTREbnfcoolK5hso5qjGZMaSYJucNTI5yEd5oNpnuCCudz5bwBN7k6dtODXBmuSsucOnfusYQr4EIZfcIJsfaPr-0VYoArQUfmFk4jj0ATkNUTCW-Pu5fLbjWKxC6ZJ_M4-Raf3-a8ipgQILzH74w0YT9_RuUl8';
  }

  private getMockCredentials(): IAuthenticationCredentials {
    return {
      region : 'au',
      firmId : '3edc3332-09f9-4302-8410-5295b6d2a928',
      brand : 'TITLEX',
      environment : 'test',
      token : {},
      userInfo : {
        username : 'Unknown username',
        userId : 'c4a3ddaf-ca2d-48a6-8967-2a0b6d82693b',
        staffId : 'b3c0a4f1-fc6d-4547-ae6f-dc902cd4d196',
        permissions : [],
        staff : {
          __id : 'b3c0a4f1-fc6d-4547-ae6f-dc902cd4d196',
          __className : 'Staff',
          deleteCode : 0,
          userId : 'c4a3ddaf-ca2d-48a6-8967-2a0b6d82693b',
          branch : '86cd8524-c8ae-44f7-b344-12bfa276ba21',
          firstName : 'John',
          lastName : 'Smith',
          middleName : '8',
          initials : 'J8S',
          fullName : 'John  8 Smith',
          legalFullName : 'John  8 Smith',
          qualifications : null,
          title : 'Mr.',
          phone : null,
          fax : null,
          email : 'tx1.8@internal.au',
          immigLicense : null,
          certificates : null,
          extension : null,
          mobile : null,
          firmReference : '',
          rate1 : 0,
          rate2 : 0,
          rate3 : 0,
          rate4 : 0,
          rate5 : 0,
          rate6 : 0,
          staffId : 'b3c0a4f1-fc6d-4547-ae6f-dc902cd4d196',
          rates : [{
            id : 1,
            name : 'A',
            rate : 0,
            display : 'A $0.00'
          }, {
            id : 2,
            name : 'B',
            rate : 0,
            display : 'B $0.00'
          }, {
            id : 3,
            name : 'C',
            rate : 0,
            display : 'C $0.00'
          }, {
            id : 4,
            name : 'D',
            rate : 0,
            display : 'D $0.00'
          }, {
            id : 5,
            name : 'E',
            rate : 0,
            display : 'E $0.00'
          }, {
            id : 6,
            name : 'F',
            rate : 0,
            display : 'F $0.00'
          }, {
            id : 0,
            name : 'Other',
            rate : 0,
            display : 'Other'
          }
          ]
        }
      },
      i18n : 'en-au'
    }
  }

}
