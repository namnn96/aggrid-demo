// core
import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor } from '@angular/common/http';
import { HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

// injectables
import { AuthService } from '@app/auth/services/auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  // internal
  private readonly log_;

  constructor(
    private authService_: AuthService
  ) {
    // this.log_ = Log.create('auth-interceptor');
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // this.log_.info(req.headers.keys().includes('Authorization'));
    let authReq = req.clone();

    if (!req.headers.keys().includes('Authorization')) { // Add token
      const authHeader = `Bearer ${this.authService_.getToken()}`;
      authReq = req.clone({setHeaders: {Authorization: authHeader}});
      // this.log_.info(`New headers: ${authReq.headers.keys()}`);
    }

    return next.handle(authReq);
  }
}
