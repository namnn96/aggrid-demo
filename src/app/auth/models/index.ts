import { IDeletable } from '@app/shared/models';

export interface IAuthenticationCredentials {
  region: string;
  firmId: string;
  brand: string;
  impersonatorId?: string;
  environment: string;
  token: any;
  userInfo?: IUserInfo;
  remembered?: boolean;
  i18n: string;
}

export interface IUserInfo {
  username: string;
  userId: string;
  staffId: string;
  staff?: IStaff;
  permissions?: Array<string>;
}

export interface IStaff extends IDeletable {
  staffId: string;
  userId: string;
  firstName: string;
  lastName: string;
  middleName: string;
  initials: string;
  email: string;
  phone: string;
  rates: IRate[];
  __id: string;
  __className: string;
  branch: string;
  fullName: string;
  legalFullName: string;
  qualifications: string;
  title: string;
  fax: string;
  immigLicense: string;
  certificates: string;
  extension: string;
  mobile: string;
  firmReference: string;
  rate1: number;
  rate2: number;
  rate3: number;
  rate4: number;
  rate5: number;
  rate6: number;
}

export interface IRate {
  id: number;
  name: string;
  rate: number;
  display: string;
}
