import { NgModule } from '@angular/core';
import { Routes, RouterModule, NoPreloading } from '@angular/router';

const routes: Routes = [
  {
    path: 'matters',
    loadChildren: './matter-list/matter.module#MatterModule',
    data: {
      breadcrumb: 'Matter'
    }
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'matters'
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: 'matters'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      routes,
      {
        preloadingStrategy: NoPreloading
      }
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
