export interface IDeletable {
  deleteCode?: number;
}

export interface IVersionable {
  version: number;
}

export interface IContextMenuItem {
  title: string;
  action: (params: any) => any;
}
