"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
/*
 * Text snippet
*/
var SnippetPipe = (function () {
    function SnippetPipe() {
    }
    SnippetPipe.prototype.transform = function (input, length, append) {
        if (input && (typeof input === 'string')) {
            length = length || 300;
            append = append || '...';
            append = (input.length > length ? append : '');
            if (input.length <= length) {
                return input;
            }
            input = input.substr(0, length);
            if (input.lastIndexOf('&#32;') !== -1) {
                return input.substr(0, input.lastIndexOf('&#32;')) + append;
            }
            else if (input.lastIndexOf(' ') !== -1) {
                return input.substr(0, input.lastIndexOf(' ')) + append;
            }
            else {
                return input + append;
            }
        }
        return input;
    };
    return SnippetPipe;
}());
SnippetPipe = __decorate([
    core_1.Pipe({ name: 'snippet' })
], SnippetPipe);
exports.SnippetPipe = SnippetPipe;
//# sourceMappingURL=snippet.pipe.js.map