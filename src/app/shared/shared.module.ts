import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ElementResizeDirective, PreventDefaultDirective, AutofocusDirective } from './directives';
import { SnippetPipe, StripFrontQuestionMarkPipe } from './pipes';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    ElementResizeDirective,
    PreventDefaultDirective,
    AutofocusDirective,
    SnippetPipe,
    StripFrontQuestionMarkPipe
  ],
  exports: [
    ElementResizeDirective,
    PreventDefaultDirective,
    AutofocusDirective,
    SnippetPipe,
    StripFrontQuestionMarkPipe
  ],
  providers: [],
})
export class SharedModule { }
