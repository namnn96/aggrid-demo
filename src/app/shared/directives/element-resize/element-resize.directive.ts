import { Directive, OnInit, ElementRef } from '@angular/core';

@Directive({
  selector: '[leapElementResize]'
})
export class ElementResizeDirective implements OnInit {

  constructor(private elementRef: ElementRef) { };

  ngOnInit(): void {
    console.log('ElementResizeDirective called');
  }

}
