export const GroupByNone = { id: 1, value: 'Ungroup' };
export const GroupByStatus = { id: 2, value: 'Status' };
export const GroupByClient = { id: 3, value: 'Client' };
export const GroupByStaff = { id: 4, value: 'Staff' };

export enum EMattersFilter {
  NonArchived = 1,
  Current = 2,
  Recent = 3,
  MyCurrent = 5,
  MyResponsible = 6,
  MyActing = 7,
  MyCredit = 8,
  MyAssisting = 9,
  DebtorBalance = 10,
  GeneralTrust = 11, // General Trust Balances
  TrustInvestment = 12, // Controlled / Investment Balances
  PowerMoney = 18,
  TransitMoney = 19,
  UnbilledTimeAndFees = 13, // Unbilled Time & Fees
  UnbilledCostRecoveries = 14, // Unbilled Cost Recoveries
  UnbilledPayments = 15, // Unbilled Office Payments
  UnbilledJournals = 16, // Unbilled Disbursement Journals
  UnbilledAnticipatedPayments = 17, // Unbilled Anticipated Payments
  AdvancedSearch = 20
}

export enum ENewMatterMode {
  SAME_TYPE = 1,
  SAME_CLIENT = 2
}

export enum SearchTermMatchType {
  Partial = 1,
  Exact = 2
}

export type MatterSearchField = 'actinguserid' | 'archivenumber' | 'assistinguserid' | 'credituserid' | 'filenumber'
  | 'firmid' | 'instructiondate' | 'isarchived' | 'iscurrent' | 'matterstatus' | 'responsibleuserid'
  | 'staffintials' | 'state' | 'mattertype' | 'customdesc' | 'filenumber' | 'firstdesc' | 'seconddesc';
