import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { IMatterListEntry, IMatterListResponseSchema } from '@app/matter-list/models';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map, tap } from 'rxjs/operators';

@Injectable()
export class MatterListService {
  // public
  matters$: Observable<IMatterListEntry[]>;

  // internal
  private matters_ = new BehaviorSubject<IMatterListEntry[]>(null);
  private path_: string;
  private lastRowVer: number = 0;

  constructor(private http: HttpClient) {
    this.path_ = `/api/docs/api/v1/matters?lastRowVer=`;
    this.matters$ = this.matters_.asObservable();
  }

  getMatters(): Observable<IMatterListEntry[]> {
    const url = this.path_ + this.lastRowVer;
    const project = (response: IMatterListResponseSchema) => response.data;
    return this.http.get(url).pipe(
      map(project),
      tap(matterList => this.matters_.next(matterList))
    );
  }

}
