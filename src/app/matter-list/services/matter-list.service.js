"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
// core
var core_1 = require("@angular/core");
var BehaviorSubject_1 = require("rxjs/BehaviorSubject");
require("rxjs/add/operator/map");
var http_1 = require("@angular/common/http");
var base_service_1 = require("../../../../shared/services/base/base.service");
var MatterListService = (function (_super) {
    __extends(MatterListService, _super);
    function MatterListService(http) {
        var _this = _super.call(this) || this;
        _this.http = http;
        // internal
        _this.matters_ = new BehaviorSubject_1.BehaviorSubject(null);
        _this.path_ = _this.apiPath + "/api/v1/matters?lastRowVer=0";
        _this.matters$ = _this.matters_.asObservable();
        return _this;
    }
    MatterListService.prototype.getMatters = function () {
        var headers = new http_1.HttpHeaders();
        headers = headers.set('Authorization', this.getToken());
        var project = function (response) { return response.data; };
        return this.http.get(this.path_, { headers: headers }).map(project);
    };
    MatterListService.prototype.getToken = function () {
        return 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE1MDkwNzE1NDIsImV4cCI6MTUxMTY2MzU0MiwiZmlybUlkIjoiM2Vk' +
            'YzMzMzItMDlmOS00MzAyLTg0MTAtNTI5NWI2ZDJhOTI4IiwidXNlcklkIjoiYzRhM2RkYWYtY2EyZC00OGE2LTg5NjctMmEwYjZkODI2OTNiIiwic' +
            '3RhZmZJZCI6ImIzYzBhNGYxLWZjNmQtNDU0Ny1hZTZmLWRjOTAyY2Q0ZDE5NiIsInNlc3Npb25JZCI6Inp4NlZEOF8yMTY2Mzg2Zi1lOTRkLTQ5NG' +
            'UtOTM3NC1lYTI1ZWZlYTAwMDAiLCJzY29wZSI6IioiLCJicmFuZCI6IlRJVExFWCIsInJlZ2lvbiI6ImF1IiwiZW52aXJvbm1lbnQiOiJ0ZXN0Iiw' +
            'idXNlclR5cGUiOiJUaXRsZVgiLCJwcm92aWRlciI6InRpdGxleC11c2VyIiwibGlua2VkVXNlcnMiOlt7ImlkIjoiM2ZlZWI1Y2EtZjkwMC00NmUy' +
            'LTllOTItMzk2YWEyMDQyZmI1IiwidXNlclR5cGUiOiJBenVyZSIsInByb3ZpZGVyIjoiYXp1cmUtdXNlciJ9XSwiYXBpX3Blcm1zIjpbeyJyIjoiL' +
            '2FwaS92MS9jYXJkcyoiLCJtIjpbIkdFVCIsIlBPU1QiXX0seyJyIjoiL2FwaS92Mi9jYXJkcyoiLCJtIjpbIkdFVCIsIlBPU1QiXX0seyJyIjoiL2' +
            'FwaS92MS9kb2N1bWVudHMqIiwibSI6WyJHRVQiLCJQT1NUIiwiUFVUIl19LHsiciI6Ii9hcGkvdjEvbGlzdHMqIiwibSI6WyJHRVQiXX0seyJyIjo' +
            'iL2FwaS92MS9ldmFsdWF0ZSoiLCJtIjpbIlBPU1QiXX0seyJyIjoiL2FwaS92MS9tYXR0ZXJzKiIsIm0iOlsiR0VUIiwiUFVUIiwiUE9TVCJdfSx7' +
            'InIiOiIvYXBpL3YxL3BlcnNvbnMqIiwibSI6WyJHRVQiLCJQVVQiLCJQT1NUIl19LHsiciI6Ii9hcGkvdjIvbWF0dGVycyIsIm0iOlsiR0VUIiwiUE' +
            '9TVCJdfV0sImF1ZCI6Ik5CWFZESlpPN1BISFQ3RkoiLCJpc3MiOiJhdXRoLXRlc3QubGVhcC5zZXJ2aWNlcyIsInN1YiI6ImM0YTNkZGFmLWNhMmQ' +
            'tNDhhNi04OTY3LTJhMGI2ZDgyNjkzYiIsImp0aSI6Ilp2d0djM0Z6YlI1dFdyM1MifQ.R-feUqDvjzQXjy0Gg8SFaNrR2zkH5BgoGt09Seu0LxJdG' +
            '5wbEZSsde_kLkesMyh-6yIgDYQg2YqT4_qg3oBWWeJhnwbhizlYumoubT1KwcuebqvVv5J86hHivf4oBt3dYA0oYkWU5kI3_7NhXwCAfhT0mB2nKXW1FlRVtXj9aoU';
    };
    return MatterListService;
}(base_service_1.BaseService));
MatterListService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.HttpClient])
], MatterListService);
exports.MatterListService = MatterListService;
//# sourceMappingURL=matter-list.service.js.map