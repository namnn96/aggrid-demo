"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var matter_list_service_1 = require("../../services/main/matter-list.service");
var index_1 = require("../../../../shared/components/index");
var MatterListComponent = (function () {
    function MatterListComponent(matterListService) {
        this.matterListService = matterListService;
        this.gridOptions = {};
    }
    MatterListComponent.prototype.ngOnInit = function () {
        var _this = this;
        var mattersStorage = localStorage.getItem('matters');
        if (mattersStorage) {
            this.matterEntries = JSON.parse(mattersStorage);
        }
        else {
            this.mattersSub = this.matterListService.getMatters()
                .subscribe(function (matterEntries) {
                _this.matterEntries = matterEntries;
                localStorage.setItem('matters', JSON.stringify(_this.matterEntries));
            });
        }
        this.gridOptions = this.getGridOptions();
        this.gridOptions.columnDefs = this.buildColumns();
    };
    MatterListComponent.prototype.onSelect = function (matterEntry) {
        this.selectedMatterEntry = matterEntry;
        console.log(matterEntry.matterId + " clicked");
    };
    MatterListComponent.prototype.isLegalAid = function (row) {
        return !!row.billingMode;
    };
    MatterListComponent.prototype.getGridOptions = function () {
        return {
            enableSorting: true,
            suppressMovableColumns: true,
            suppressHorizontalScroll: true,
            icons: {
                sortAscending: '<i class="tk-icon-triangle-down"/>',
                sortDescending: '<i class="tk-icon-triangle-up"/>',
            },
            overlayLoadingTemplate: index_1.generatePreloaderMarkup({
                background: 'white',
                opacity: 'transparent',
                position: 'absolute'
            }),
            overlayNoRowsTemplate: index_1.generateNoResultsMarkup({
                message: 'No taxcodes found'
            })
        };
    };
    MatterListComponent.prototype.buildColumns = function () {
        return [
            {
                headerName: 'No.',
                field: 'fileNumber',
                headerClass: 'matterNumber col-center width-14' + this.isDefaultSort ? ' hide-sort-icon' : '',
                cellRenderer: function (params) {
                    return "<td width=\"14%\" class=\"matternumber col-center\">\n                <strong>\n                  {{params.fileNumber}}\n                </strong>\n              </td>";
                }
            }
            // , {
            //   headerName: 'Client & Details',
            //   headerClass: 'clientAndDetails',
            //   field: 'RatePercent',
            //   cellClass: () => 'col-center',
            //   cellRenderer: (params) => {
            //     return `<td class="firstDescription noselect">
            //           <h5 class="margin0">
            //             <strong [innerText]="row.firstDescription" data-hj-masked></strong>
            //           </h5>
            //
            //           <dl class="margin-bottom0" *ngIf="row.customDescription">
            //             <dd>
            //               <strong [innerText]="row.customDescription" data-hj-masked></strong>
            //               <span *ngIf="row.secondDescription" data-hj-masked> - {{row.secondDescription | snippet : 200}}</span>
            //             </dd>
            //           </dl>
            //
            //         </td>`;
            //   }
            // },
            // {
            //   headerName: 'GL Tax Code',
            //   headerClass: 'col-center',
            //   cellClass: 'col-center',
            //   field: 'GLTaxCode',
            //   maxWidth: 180,
            //   minWidth: 140,
            //   cellRenderer: function (params) {
            //     if (params.value) {
            //       return  `<span class="label label-primary">
            //               ${params.value}
            //             </span>`;
            //     } else {
            //       return null;
            //     }
            //   }
            // }
        ];
    };
    return MatterListComponent;
}());
MatterListComponent = __decorate([
    core_1.Component({
        selector: 'matter-list',
        templateUrl: './matter-list.component.html',
        styleUrls: ['./matter-list.component.css'],
        providers: [matter_list_service_1.MatterListService]
    }),
    __metadata("design:paramtypes", [matter_list_service_1.MatterListService])
], MatterListComponent);
exports.MatterListComponent = MatterListComponent;
//# sourceMappingURL=matter-list.component.js.map