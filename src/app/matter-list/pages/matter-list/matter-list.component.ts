import { Component, OnDestroy, OnInit } from '@angular/core';
import { CellClickedEvent, ColDef, FilterManager, GridOptions, RowClickedEvent } from 'ag-grid';
import findIndex from 'lodash-es/findIndex';
import toUpper from 'lodash-es/toUpper';
import join from 'lodash-es/join';
import { IMatterListEntry } from '@app/matter-list/models';
import { MatterListService } from '@app/matter-list/services';
import { Subject } from 'rxjs/Subject';
import { take } from 'rxjs/operators';

@Component({
  selector: 'leap-matter-list',
  templateUrl: './matter-list.component.html',
  styleUrls: [ './matter-list.component.css' ]
})
export class MatterListComponent implements OnInit, OnDestroy {

  // Matter entries
  private ngUnsubscribe$: Subject<void>;
  matterEntries: IMatterListEntry[];
  selectedMatterEntry: IMatterListEntry;

  // UI flags
  isDefaultSort: boolean;
  showState: boolean;
  showDeleted: boolean;
  hasNoResults: boolean;
  showLoadingIndicator: boolean;

  // AG Grid
  gridOptions: GridOptions;

  constructor(private matterListService: MatterListService) {
    this.ngUnsubscribe$ = new Subject<void>();
    this.gridOptions = <GridOptions> {};
  }

  ngOnInit(): void {
    this.getData();

    this.matterListService.matters$
      .subscribe((matterEntries: IMatterListEntry[]) => {
        this.matterEntries = matterEntries;
      });

    this.showState = false;
    this.showDeleted = false;
    this.hasNoResults = false;
    this.showLoadingIndicator = false;

    this.gridOptions = this.getGridOptions();
    this.gridOptions.columnDefs = this.buildColumns();
  }

  getData(): void {
    this.matterListService.getMatters().pipe(take(1)).subscribe();
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe$.next();
    this.ngUnsubscribe$.complete();
  }

  onGridReady(grid: any) {
    grid.api.sizeColumnsToFit();
  }

  onGridSizeChanged(params: any) {
    const api = this.gridOptions.api;
    if (api) {
      api.sizeColumnsToFit();
    }
  }

  isLegalAid(row: IMatterListEntry): boolean {
    return !!row && !!row.billingMode;
  }

  getGridOptions(): GridOptions {
    return <GridOptions> {
      enableSorting: true,
      groupUseEntireRow: true,
      suppressMovableColumns: true,
      suppressHorizontalScroll: true,
      icons: {
        sortAscending: '<i class="tk-icon-triangle-down"/>',
        sortDescending: '<i class="tk-icon-triangle-up"/>',
      },
      onRowClicked: (event: RowClickedEvent) => {
        this.selectedMatterEntry = event.data;
        console.log(`${this.selectedMatterEntry.matterId} clicked`);
      }
    };
  }

  buildColumns(): ColDef[] {
    return [
      {
        headerName: 'No.',
        field: 'fileNumber',
        headerClass: `matterNumber col-center width-14 ${this.isDefaultSort ? ' hide-sort-icon' : ''}`
      }, {
        headerName: 'Client & Details',
        headerClass: `clientAndDetails ${this.showState ? 'width-54' : 'width-46'}`,
        field: 'firstDescription',
        getQuickFilterText: (params) => {
          const matterEntry: IMatterListEntry = params.data;
          return toUpper(join(
            [matterEntry.firstDescription, matterEntry.customDescription, matterEntry.secondDescription],
            FilterManager.QUICK_FILTER_SEPARATOR
          ));
        }
      },
      {
        headerName: 'Staff',
        headerClass: 'staffInitials col-center width-8',
        field: 'staffInitials'
      },
      {
        headerName: 'Status',
        headerClass: 'matterStatus col-center width-18',
        field: 'matterStatus',
      },
      {
        headerName: 'State',
        headerClass: 'state col-center width-8',
        field: 'matterState',
        hide: !this.showState
      },
      {
        headerClass: 'col-center width-3',
        cellRenderer: () => `<td width='1%'></td>`,
      }
    ];
  }

  setCurrentRow($event: any, row: IMatterListEntry): void {
    if ($event) {
      $event.stopPropagation();
    }
    if (row && !row.accessible) {
      // don't allow clicking on inaccessible (locked) rows
      return;
    }
    if (row && this.isLegalAid(row)) {
      // don't allow clicking on Legal Aid rows
      return;
    }
    const focusIndex = findIndex(this.matterEntries, { matterId: row.matterId });
    this.gridOptions.api.selectIndex(focusIndex, false, null);
  }
}
