import { MatterSearchField, SearchTermMatchType } from '@app/matter-list/constants';
import { IDeletable, IVersionable } from '@app/shared/models';

export interface IMatterListResponseSchema {
  data: IMatterListEntry[];
  lastRowVer: number;
  total: number;
}

export interface IDocsCriticalDateDTO {
  id: string;
  name: string;
  date: string;
  confirmed: boolean;
  confirmStatus: string;
  order: number;
  status: string;
  isConfirmable: boolean;
}

export interface IMatterCoreCriticalDates {
  [property: string]: IDocsCriticalDateDTO;
}

export interface ISubstitution {
  client: string;
  otherSide: string;
  otherSideSolicitor: string;
  otherSideInsurer: string;
}

export interface IMatterAccountingCostAgreement {
  required: boolean;
  receivedDate: string;
}

export interface IMatterAccountingInvoiceSettings {
  fixed: boolean;
  fixedAmount: number;
}

export interface IDocsList<T extends IDeletable> {
  data: T[];
  total: number;
  lastRowVer: number;
}

export interface IMatterAccounting {
  feeEstimate: number;
  feeEstimateGST: number;
  disbursementsEstimate: number;
  disbursementsEstimateGST: number;
  showEstimateWarning: boolean;
  invoiceSettings: IMatterAccountingInvoiceSettings;
  costAgreement: IMatterAccountingCostAgreement;
  taxFree: boolean;
  debtorNote: string;
}

export enum AccessControlStaffRole {
  None = 0,
  Credit = 1,
  Responsible = 2,
  Acting = 3,
  Assisting = 4
}

export interface IAccessControlStaff {
  id: string;
  role: AccessControlStaffRole;
}

export interface IAccessControl {
  enabled: boolean;
  staffList: IAccessControlStaff[];
}

export interface IMatterCore extends IDeletable {
  cardList: string[];
  definableTableList: string[];
  __id: string;
  __className: string;
  type: string;
  fileNumber: string;
  credit: string;
  personResponsible: string;
  personActing: string;
  personAssisting: string;
  title: string;
  matterTypeId: string;
  criticalDates: IMatterCoreCriticalDates;
  __substitution: ISubstitution;
  category: string;
  dateOpened: Date;
  dateClosed: Date;
  crashDate: Date;
  exchangeDate: Date;
  completionDate: Date;
  costAgreement: boolean;
  status: string;
  costsEstimated: number;
  codeUnique: number;
  titleClient: string;
  titleClientClientAsPrefix: boolean;
  titleClientOtherSideAsSuffix: boolean;
  titleClientAsPrefix: boolean;
  titleOtherSideAsSuffix: boolean;
  titleClientUseDefault: boolean;
  titleUseDefault: boolean;
  state: string;
  description: string;
  disbursementsEstimate: number;
  percentageComplete: number;
  referrerCardId: string;
  referrerPersonId: string; // id of person or card
  sourceOfWork: string;
  accounting: IMatterAccounting;
  acl: IAccessControl;
}

export interface IMattersGroup {
  name: string;
  collection: IMatterListEntry[];
}

export interface IMattersGroupBy {
  id: number;
  value: string;
}

export interface IMatterListRow {
  matterId: string;
  isGroup?: boolean;
  groupName?: string;
  isIncludeAllMattersRow?: boolean;
  translateKey?: string;
  data?: IMatterListEntry;
  quickFilterText?: string;
  children?: IMatterListRow[];
}

export interface IMatterListEntry extends IDeletable {
  matterId: string;
  fileNumber?: string;
  fileNumberPadded?: string;
  firstDescription?: string;
  secondDescription?: string;
  customDescription?: string;
  instructionDate?: Date;
  matterType?: string;
  matterStatus?: string;
  isCurrent?: boolean;
  matterState?: string;
  staffInitials?: string;
  staffResponsible?: string;
  staffResponsibleName?: string;
  staffCredit?: string;
  staffCreditName?: string;
  staffActing?: string;
  staffActingName?: string;
  staffAssisting?: string;
  staffAssistingName?: string;
  isArchived?: boolean;
  // NB: removed rowVersion and cardIdList because they are not returned by matter search API,
  // so we can't rely on it for all matters. Current code doesn't use it, anyway.
  accessible?: boolean;
  nonExistent?: boolean;
  billingMode?: number;
  archiveNumber?: string;
}

export interface IEmptyRole {
  order: number;
  tableId: string;
}

export enum EMatterDetailType {
  Card = 1,
  DefinableTable = 2
}

export interface IMatterDescription {
  firstDescription: string;
  secondDescription: string;
}

export interface IMatterDetailEntry {
  __id?: string;
  __className?: string;
  __fileOrder: number;
  __displayOrder: number;
  __name: string;
  __description: string;
  __tableId?: string;
  tableId: string;
  subsTableId?: string;
  detailType: EMatterDetailType;
  singleton?: boolean;
  error?: string;
  hiddenOnMatter?: boolean;
  matterDescriptions?: IMatterDescription;
  isCard: boolean;
  default?: boolean;
}

export interface IDefinableTable extends IMatterDetailEntry {
  __matterId: string;
  layoutId: string;
  desc: string;
  needsContentApp?: boolean;
  basedOnCard?: boolean;
  cardId?: string;
  // fields: Array<ITableField>; // for update payload
  // __fields: Array<{
  //     fieldId: string;
  //     fieldName: string;
  // }>;
  // convenient: hold list of fieldId-fieldValue mappings so that we can construct relatedLayout object after user updating layout form
  descriptionFn?: Function;
  descriptionProp?: string;
}

export interface IMatterCard extends IMatterDetailEntry {
  cardId: string;
  cardPersonList: string[];
  __inherited: string;
  reference: string;
  relatedTable: IDefinableTable;  // for updating related definableTable of this card role
  persons: IPerson[];             // convenient: list of persons being used by this card role
  unusedPersons: IPerson[];       // convenient: list of persons in this card not being used by this card role
  isDebtor: boolean;
  isPersonCard: boolean;
  cardDefinableTable?: IDefinableTable;
  cardShortName?: string;
}

export interface IPersonCardRole {
  id: string;
  isPrimary: boolean;
}

export interface IPerson extends IDeletable, IVersionable {
  __id: string;
  __className: string;
  __firmId: string;
  userId: string;

  __personId: string;
  salutation: string;
  firstNames: string;
  lastName: string;
  email: string;
  phone: string;
  previousNames: string;
  maidenName: string;
  gender: string;
  nationality: string;
  passportNumber: string;
  passportName: string;
  dateOfBirth: Date;
  placeOfBirth: string;
  countryOfBirth: string;
  dateOfDeath: boolean;
  placeOfDeath: string;
  primary: boolean;
  webAddressList: IWebAddress[];
  phoneNumberList: IPhoneNumber[];
  cardRoles: IPersonCardRole[];
  cards: ICard[];
  primaryCards: string[]; // list of primary card ids,
  dirty: boolean;

  // TODO move to separate interface
  emails: IWebAddress[];
  web: IWebAddress;
}

export interface IWebAddress {
  __className: string;
  type: string;
  address: string;
}

export interface IPhoneNumber {
  __className: string;
  numberType: string;
  number: string;
  areaCode: string;
}

export interface ITrustee {
  __className: string;
  trusteeName: string;
  trusteeIsACompany: boolean;
  company: string;
  trusteeCrn: string;
  trusteeAcn: string;
}

export type AddressType = 'Street' | 'POBox' | 'DX';

export interface IAddress {
  __className: string;
  addressType: AddressType;
  type: AddressType;
  propertyBuildingName: string;
  levelUnitShop: string;
  number: string;
  street: string;
  suburb: string;
  county: string;
  state: string;
  postcode: string;
  country: string;
  specialInstructions: string;
  // exchange: string;
  // dateFrom: Date;
  // dateTo: Date;
  // formatted: string;
  // full: string;
}

export interface IBusiness {
  companyOwnerName: string;
  businessType: string;
  abn: string;
  crn: string;
  tin: string;
}

export interface ICompany {
  companyOwnerName: string;
  companyTitle: string;
  tradingTitle: string;
  businessType: string;
  abn: string;
  crn: string;
  tin: string;
}

export interface IGovernment {
  companyOwnerName: string;
  abn: string;
  crn: string;
  tin: string;
}

export interface ITrust {
  dateOfTrust: Date;
  trustStatus: string;
  trusteeList: ITrustee[];
}

export interface IOrganisation extends IBusiness, ICompany, IGovernment, ITrust {
  tradingNameTrustName: string;
  propertyBuildingName: string;
  soleDirectorCompany: boolean;
}

export interface ICardRoles {
  tables: string[];
  subsTables: string[];
}

export type ICardType = 'Trust' | 'People' | 'Organisation' | 'Business' | 'Government' | 'Company';

export interface ICardDTO extends IOrganisation, IDeletable, IVersionable {
  __id: string;
  __className: string;
  __firmId: string;
  description?: string;
  title?: string;
  shortName?: string;
  fullName?: string;
  cardType: ICardType;
  type: ICardType;
  isSupplier: boolean;

  personList: IPerson[];

  // Address lists
  originalAddressList: IAddress[];
  defaultOriginalAddress?: string;
  alternativeAddressList: IAddress[];
  defaultAlternativeAddress?: string;

  // Phone lists
  phoneNumberList: IPhoneNumber[];

  // Emails
  webAddressList: IWebAddress[];
  webAddress: IWebAddress;

  // Letter
  userTitle: string;
  useDefaultTitle: boolean;
  useFriendlyDear: boolean;
  userDear: string;
  useDefaultDear: boolean;

  // Comment
  comments: string;

  bankAccount?: {
    bsb?: string;
    accountName?: string;
    accountNumber?: string
  };

  roles: ICardRoles;

  // for card entry
  __cardId: string;
  reference: string; // For matter card

  relatedTables?: IDefinableTable[];
}

export interface ICard extends ICardDTO {
}

export interface IMatter extends IDeletable {
  matter: IMatterCore;
  emptyCardRoles: IEmptyRole[];
  emptyDefinableTables: IEmptyRole[];
  matterCards: IMatterCard[];
  definableTables: IDefinableTable[];
  cards: ICard[];
  people: IPerson[];

  // for matter entry
  matterId: string;
  fileNumber: string;
  firstDescription: string;
  secondDescription: string;
  customDescription: string;
  matterType: string;
  matterStatus: string;
  matterState: string;
  isCurrent: boolean;
  staffInitials: string;
  staffResponsible: string;
  staffResponsibleName: string;
  staffCredit: string;
  staffCreditName: string;
  staffActing: string;
  staffActingName: string;
  staffAssisting: string;
  staffAssistingName: string;
  isArchived: boolean;
  cardIdList: string[];

  corrupted?: boolean;
}

export interface AdvancedSearchData {
  searchTerms: SearchTerm[];
  includeArchivedMatters?: boolean;
}

export interface SearchTerm {
  termName: MatterSearchField;
  termValue: string | number | Date;
  termOption?: SearchTermMatchType;
}

