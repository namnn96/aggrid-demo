"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AccessControlStaffRole;
(function (AccessControlStaffRole) {
    AccessControlStaffRole[AccessControlStaffRole["None"] = 0] = "None";
    AccessControlStaffRole[AccessControlStaffRole["Credit"] = 1] = "Credit";
    AccessControlStaffRole[AccessControlStaffRole["Responsible"] = 2] = "Responsible";
    AccessControlStaffRole[AccessControlStaffRole["Acting"] = 3] = "Acting";
    AccessControlStaffRole[AccessControlStaffRole["Assisting"] = 4] = "Assisting";
})(AccessControlStaffRole = exports.AccessControlStaffRole || (exports.AccessControlStaffRole = {}));
var EMatterDetailType;
(function (EMatterDetailType) {
    EMatterDetailType[EMatterDetailType["Card"] = 1] = "Card";
    EMatterDetailType[EMatterDetailType["DefinableTable"] = 2] = "DefinableTable";
})(EMatterDetailType = exports.EMatterDetailType || (exports.EMatterDetailType = {}));
//# sourceMappingURL=index.js.map