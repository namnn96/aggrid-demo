import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '../shared/shared.module';

import { MatterRoutingModule } from './matter-routing.module';
import { AgGridModule } from 'ag-grid-angular/main';
import { agGridComponents } from './components';
import { MatterListComponent } from './pages';

import { MatterListService } from './services';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AuthInterceptor } from '@app/auth/interceptors/AuthInterceptor';

@NgModule({
  imports: [
    HttpClientModule,
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    MatterRoutingModule,
    AgGridModule.withComponents(agGridComponents)
  ],
  entryComponents: [
  ],
  declarations: [
    MatterListComponent
  ],
  exports: [],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    MatterListService
  ],
})
export class MatterModule {
}
