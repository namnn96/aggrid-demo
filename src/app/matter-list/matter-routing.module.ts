import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MatterListComponent } from './pages';

const routes: Routes = [
  {
    path: '',
    component: MatterListComponent,
    data: {
      breadcrumb: 'Matters'
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MatterRoutingModule { }
